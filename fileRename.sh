#!/bin/bash

target=$1
replace=$2
dir=`pwd`
arrayDir=()
action=""


# get path in parameter $1 for file or directory
changeName(){
	# i get only the last part of path as the name
	newName=`echo ${1##*/} | tr $target $replace`

	# if the new and old name are differnt, i change the name whit "mv"
	if [ "${1##*/}" != "$newName" ]; then
		if [ "$2" == "o" ]; then
			echo -e "${1} => ${1%%${1##*/}}${newName}\n"
			echo -e "${1},${1%%${1##*/}}${newName}" >> report.csv
		else
			echo -e "\n${1} => renommé"
			mv "${1}" "${1%%${1##*/}}${newName}"
		fi
	fi
}


dirFunc() {
	# parcourir le repertoire
	for file in ls "$1/"*
	do
		if [ -f "$file" ]; then
			# call the change name function
			changeName $file $action

		elif [ -d "$file" ]; then
			# i save the dir path
			arrayDir+=($file)
			# call the change name function
			dirFunc $file
		fi
	done
}

selection() {
	while true
	do
		read -p "Voulez-vous lancer l'action en test? o/n " action
		if [ $action == "n" ]; then
			read -p "Vous avez choisi de lancer le script en production. Vous validez? o/n " action
			if [ $action == "o" ]; then
				action="n"
				break
			elif [ $action == "n" ]; then
				echo -e "\n### Vous avez annulé le lancement en production. ###\n"
			fi
		elif [ $action == "o" ]; then
			break
		elif [ $action != "n" ]; then
			echo -e "\n### Erreur de saisie. Merci de saisir 'o' ou 'n' ###\n"
		fi
	done
	clear
	echo -e "\n\t#####\t FILE RENAME SCRIPT IN ACTION \t#####\n"
}

# Select the execution mode
selection

# at first time, i change the file's name
echo -e "NOM D'ORIGINE,NOM MODIFIÉ" > report.csv
dirFunc $dir

# at second time, i change the directories's name


i=${#arrayDir[@]}
until [ $i -lt 0 ]
do
  changeName ${arrayDir[${i}]} $action
  ((i--))
done